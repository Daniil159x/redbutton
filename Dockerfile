FROM python:3.7-alpine

ADD . .

CMD [ "python", "-u", "./main.py" ]
