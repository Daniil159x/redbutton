

class Team:
    """Класс содержит информацию по команде и хендлер, который отвечает за отправку событий на удалённый сервис"""
    def __init__(self, info, handler):
        self._id = info[0]
        self._handler = handler
        self._handler.register_class(self)
        self._name = info[1]
        self._btn_idx = info[2]
        self._core = None

    def cleanup(self):
        self._handler.unregister_class(self)

    def set_core(self, core):
        self._core = core

    def set_handler(self, handler):
        if self._handler is not None:
            self._handler.unregister_class(self)
        self._handler = handler
        self._handler.register_class(self)

    def get_handler(self):
        return self._handler

    def alert(self, timestamp):
        self._core.on_alert(self, timestamp)

    def set_state(self, state):
        self._handler.set_state(self, state)

    def get_state(self):
        return self._core.on_state_request(self)

    def btn(self):
        return self._btn_idx

    def get_id(self):
        return self._id

    def set_name(self, name):
        self._name = name

    def get_name(self):
        return self._name
