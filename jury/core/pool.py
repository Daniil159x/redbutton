import threading
import uuid
from ..core import log as jury_log
from typing import List, Dict
from datetime import datetime

logger = jury_log.get_logger("ThreadPool")
timer_counter = 0


class RepeatTimer:
    """Повторяющийся таймер"""
    def __init__(self, timeout, func, pool, name="No name timer"):
        global timer_counter

        self._myid = timer_counter
        timer_counter += 1

        self.timeout = timeout
        self.func = func
        self.stop_event = threading.Event()
        self.name = f"{self._myid}: {name}"
        self._thread = None
        self._last_trigger = None
        self._pool = pool

    def start(self, args=None, kwargs=None):
        self.stop_event.clear()
        if args is None:
            args = ()
        if kwargs is None:
            kwargs = {}
        self._thread = self._pool.launch_thread(target=self.__loop, args=args, kwargs=kwargs, name=self.name)

    def stop(self):
        self.stop_event.set()

    def get_last_time_trigger(self):
        return self._last_trigger

    def is_launched(self):
        return self._thread.is_alive()

    def get_id(self):
        return self._myid

    def __loop(self, *args, **kwargs):
        while not self.stop_event.wait(self.timeout):
            self._last_trigger = datetime.now()
            self.func(*args, **kwargs)


class ThreadPool:
    """Реализация пула питоновских тредов для разных блокирующих операций"""
    def __init__(self):
        self._threads = []  # type: List[threading.Thread]
        self._timers = {}  # type: Dict[int, RepeatTimer]
        # self._exit_event = threading.Event()

    def launch_thread(self, *args, **kwargs):
        """Создать, запустить и вернуть поток"""
        t = threading.Thread(*args, **kwargs)
        t.start()
        logger.info(f"thread '{t.name}' launched")
        self._threads.append(t)
        return t

    def join(self):
        """Ждать исполнение всех потоков"""
        for t in self._threads:
            if t.is_alive() and t != threading.currentThread():
                logger.info(f"join thread '{t.name}'")
                t.join()
        self._threads.clear()

    def create_timer(self, timeout, func, name=None, args=None, kwargs=None) -> RepeatTimer:
        """Запуск таймера"""
        if name is None:
            name = f"Timer({timeout} s) for {func.__name__}'"
        else:
            name = f"Timer '{name}'"

        timer = RepeatTimer(timeout, func, self, name=name)
        logger.debug(f"Таймер '{name}' создан")

        self.register_timer(timer)
        return timer

    def register_timer(self, timer):
        """Зарегистрировать таймер в пуле"""
        timer_id = timer.get_id()
        if timer_id in self._timers:
            logger.warning(f"Таймер '{timer.name}' не зарегистирован, так как Timer с id '{timer_id}' уже есть")
            return

        self._timers[timer_id] = timer
        logger.debug(f"Таймер '{timer.name}' зарегистирован")

    def unregister_timer(self, timer):
        """Удалить таймер из пула"""
        logger.debug(f"Таймер '{timer.name}' удалён")
        timer_id = timer.get_id()
        if timer_id in self._timers:
            self._timers[timer_id] = timer
        else:
            logger.debug(f"Таймер '{timer.name}' не найден")

    def get_list_threads(self) -> List[threading.Thread]:
        return self._threads

    def get_timer_dict(self) -> Dict[int, RepeatTimer]:
        return self._timers

    def stop(self):
        """Остановить все потоки и таймеры"""
        for _, timer in self._timers.items():
            timer.stop()
