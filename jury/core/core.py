# -*- coding: utf-8 -*-

from .. import config
from typing import Dict
from .team import Team
from datetime import datetime
from jury_sdk import protocol
from ..core import log as jury_log

logger = jury_log.get_logger("Core")


class Core:
    """Класс ядра журейки, тут сосредоточена её логика"""

    def __init__(self, teams: Dict[int, Team], loger):
        self._hard = None
        self._teams = teams
        self._states = {}
        # for key in self._team:
        #     self._states[key] = protocol.State.UNLOCK
        self._loger = loger

        for t in self._teams:
            self._teams[t].set_core(self)

    def set_hard(self, hard):
        """Установить модуль/прокси железа"""
        self._hard = hard
        for team_id, tm in self._teams.items():
            self._hard.turn_off_led(tm.btn())
            self._states[team_id] = protocol.State.UNLOCK

    def on_click(self, idx, hard_state):
        """Меняет стейт и передаёт управление конкретной команде"""
        team_id = self.__idx_to_team_id(idx)
        state = self.__hard_state_to_protocol(hard_state)  # TODO: концептуально неверно, ибо нажатие на кнопку - это уже UNLOCK
        state = protocol.State.UNLOCK

        logger.info(f"on_click btn: {idx}, old state: {self._states[team_id]}, new state: {state}, team id: {team_id}")

        self._states[team_id] = state
        self._loger.on_click(team_id, state)
        self._teams[team_id].set_state(state)
        self._hard.turn_off_led(self._teams[team_id].btn())

    def on_state_request(self, team: Team):
        """Возвращает состояние команды"""
        self._loger.on_state_request(team)
        state = self._states[team.get_id()]

        logger.info(f"on_state_request team id: {team.get_id()}, state: {state}")

        return state

    def on_alert(self, team: Team, timestamp: datetime):
        """Включает кнопку, меняет статус, передаёт управление конкретной команде"""
        new_state = protocol.State.LOCK
        team_id = team.get_id()

        logger.info(f"on_alert team id: {team_id}, old state: {self._states[team_id]}, new state: {new_state}")

        self._loger.on_alert(team, timestamp)
        self._hard.turn_on_led(team.btn())
        self._states[team_id] = new_state
        team.set_state(new_state)

    def get_handler_table(self):
        return (self.on_state_request, self.on_alert)

    @staticmethod
    def __idx_to_team_id(idx):
        return config.Team.TEAMS[idx]

    @staticmethod
    def __hard_state_to_protocol(state):
        if state:
            return protocol.State.LOCK
        else:
            return protocol.State.UNLOCK
