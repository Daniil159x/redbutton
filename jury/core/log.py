#!/usr/bin/env python3

from .. import config as jury_config

import logging

formatter = logging.Formatter('%(asctime)s [%(name)s][%(levelname)s] %(message)s', '%H:%M:%S')

fh = logging.FileHandler(jury_config.Logger.LOG_FILE_NAME, mode=jury_config.Logger.LOG_FILE_MODE)
fh.setFormatter(formatter)


def get_logger(name: str, lvl=jury_config.Logger.DEFAULT_LOGGER_LEVEL) -> logging.Logger:
    logger = logging.getLogger(name)
    logger.setLevel(lvl)
    logger.addHandler(fh)
    return logger
