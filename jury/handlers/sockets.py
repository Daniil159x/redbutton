import socket
from jury_sdk import protocol
from typing import Dict, Set, List
from .. import config as jury_config
from ..core import log as jury_log

logger = jury_log.get_logger("SocketServer")


class SocketServer:
    def __init__(self, pool):
        self._listen_socket = socket.socket()
        self._listen_socket.settimeout(2)  # 2 секунды
        self._teams = {}  # type: Dict[int, Team]
        self._sockets = {}  # type: Dict[int, List[socket.socket]]
        self._team_sockets = {}  # type: Dict[socket.socket, Set[int]]
        self._thread = None
        self._has_stop = False
        self._pool = pool

    def register_class(self, team):
        logger.info(f"register class team: {team.get_id()}")
        self._teams[team.get_id()] = team

    def unregister_class(self, team):
        logger.info(f"unregister class team: {team.get_id()}")
        self._teams.pop(team.get_id())

    def get_sockets_team(self, team):
        if team in self._sockets:
            return self._sockets[team]
        else:
            return []

    def set_state(self, team, state: protocol.State):
        """Передать сервису, отвечающий за команду, новое состояние"""
        team_id = team.get_id()
        try:
            if team_id not in self._sockets:
                logger.warning(f"Team id '{team_id}' not connected'")
                return
            msg = protocol.Protocol.set_state(team_id, state)

            for soc_to in self._sockets[team_id]:
                logger.debug(f"{{jury}}->{soc_to.getpeername()}: {msg}")
                soc_to.send((msg + '\n').encode())

        except Exception as e:
            logger.warning(f'exception it = [{team_id}] {e}')

    def run(self):
        """Запустить хендлер, его обработчик событий"""
        logger.info(f"run")
        self._has_stop = False
        self._thread = self._pool.launch_thread(target=self.__loop__, name="SocketServer_listen")

    def stop(self):
        """Остановить хендлер, обработчики сообщений"""
        logger.info(f"stop")
        self._has_stop = True
        msg = protocol.Protocol.finished()

        dump = self._team_sockets
        self._team_sockets = {}
        for s in dump:
            logger.debug(f"{{jury}}->{s.getpeername()}: {msg}")
            s.send((msg + '\n').encode())
            SocketServer.__wrap_shutdown(s)
        logger.info(f"finished sended")

    def __loop__(self):
        logger.info(f"binding...")
        self._listen_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self._listen_socket.bind(('', jury_config.SocketHandler.BIND_PORT))

        logger.info(f"listening...")
        self._listen_socket.listen(1)
        while not self._has_stop:
            try:
                conn, addr = self._listen_socket.accept()
                conn.settimeout(2)
                logger.info(f"accept {addr}")
                self._pool.launch_thread(target=self.__recv_socket, args=(conn,), name=f"SocketServer_{conn.getpeername()}")

                logger.info(f"listening...")
                self._listen_socket.listen(1)
            except socket.timeout:
                pass
            except Exception as e:
                logger.error(f"accepting: {e}")

                if not self._has_stop:  # FIXME: что за жесть
                    logger.info(f"listening...")
                    self._listen_socket.listen(1)

    def __recv_socket(self, conn: socket.socket):
        name = conn.getpeername()
        while not self._has_stop:
            try:
                msg = conn.recv(1024)

                logger.debug(f"{name}->{{jury}}: {msg}")

                # empty messege
                if msg is None or len(msg) == 0 or self._has_stop:
                    self.__drop_conn(conn)
                    break

                msg = msg.decode()

                for part in msg.splitlines():
                    obj = protocol.Protocol.parse(part)
                    self.__process_obj(conn, obj)

                # если первым сообщением сервис не зарегал тиму
                # или был удалён, то заканчиваем этот тред
                if conn not in self._team_sockets:
                    break

            except socket.timeout:
                # logger.debug("[DEBUG] timeout")
                pass
            except protocol.ProtocolError as e:
                err_msg = protocol.Protocol.error(e.code, str(e))
                logger.info(f"protocol error: {err_msg}")
                conn.send((err_msg + '\n').encode())
                # а если это FORBIDDEN и нет других регистраций, то в топку его
                if e.code == protocol.ErrorCode.FORBIDDEN:
                    logger.warning(f"from {name} FORBIDDEN request: {e}")
                    if conn not in self._team_sockets or len(self._team_sockets) == 0:
                        break
            except Exception as e:
                logger.error(f"__recv_socket error: {e}")
                err_msg = protocol.Protocol.error(protocol.ErrorCode.INTERNAL_ERROR, str(e))
                count = conn.send((err_msg + '\n').encode())
                logger.error(f"__recv_socket sended {count} error bytes")

        logger.info(f"{name} left")
        SocketServer.__wrap_shutdown(conn)

    def __process_obj(self, conn, obj):
        if obj['method'] == protocol.Method.STATE_REQUEST:
            self.__state_requst(conn, obj)
        elif obj['method'] == protocol.Method.ALERT:
            self.__alert(conn, obj)
        else:
            raise KeyError("Method not support by server")

    def __state_requst(self, conn, obj):
        team_id = obj['team']
        if team_id not in self._teams:
            raise protocol.ProtocolError(protocol.ErrorCode.BAD_REQUEST, f"Team '{team_id}' is not valid")

        if team_id not in self._sockets:
            # NOTE: добавляем поддержку множественных коннектов для тимы
            self._sockets[team_id] = []

        if conn not in self._team_sockets:
            self._team_sockets[conn] = set()

        self._team_sockets[conn].add(team_id)
        self._sockets[team_id].append(conn)

        state = self._teams[team_id].get_state()
        msg = protocol.Protocol.set_state(team_id, state)

        logger.debug(f"{{jury}}->{conn.getpeername()}: {msg}")
        conn.send((msg + '\n').encode())

    def __alert(self, conn, obj):
        team_id = obj['team']
        if team_id not in self._teams:
            raise protocol.ProtocolError(protocol.ErrorCode.BAD_REQUEST, "Team id is not valid")
        self._teams[team_id].alert(obj['timestamp'])

    def __drop_conn(self, conn: socket.socket, shutdown=True):
        if conn in self._team_sockets:
            set_team_id = self._team_sockets.pop(conn)
            for team_id in set_team_id:
                if team_id in self._sockets:
                    self._sockets[team_id].remove(conn)

        if shutdown:
            SocketServer.__wrap_shutdown(conn)

    def __check_conn_alive(self, conn: socket.socket):
        return True

    @staticmethod
    def __wrap_shutdown(soc: socket.socket, how=socket.SHUT_RDWR):
        try:
            soc.shutdown(how)
        except:  # noqa
            pass
