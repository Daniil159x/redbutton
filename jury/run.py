#!/usr/bin/env python3

import signal
import sys
import time
import os
from datetime import datetime
from typing import Dict, List

from . import config as jury_config  # TODO: конфигурацию тоже надо выделить из пакета jury
sys.path.append(jury_config.Global.REPOSITORY_ROOT_PATH)  # NOTE: костыль для дальнейшего импорта sdk

from . import core as jury_core  # noqa
from . import web as jury_web  # noqa
from . import handlers as jury_handlers  # noqa
from .core import log as jury_log  # noqa
from . import cli as jury_cli  # noqa
from . import flag_submitter as jury_flag  # noqa


logger = jury_log.get_logger("main")


def create_teams(handlers) -> Dict[int, jury_core.Team]:
    teams = {}
    for i, team_id in enumerate(jury_config.Team.TEAMS):
        # info[0] = id
        # info[1] = name
        # info[2] = btn idx
        info = (
            team_id,
            jury_config.Team.TEAM_COLLECTION[team_id],
            i,
        )
        type_handl = jury_config.Team.TEAM_HANDLERS_COLLECTION[team_id]
        teams[team_id] = jury_core.Team(info, handlers[type_handl])

    return teams


def create_handlers(web, pool) -> Dict[int, jury_handlers.SocketServer]:
    types = set()

    handlers = {}

    for _, t in jury_config.Team.TEAM_HANDLERS_COLLECTION.items():
        types.add(t)

    for t in types:
        if t == jury_config.HandlersType.SOCKET:
            handlers[t] = jury_handlers.SocketServer(pool)
        else:
            raise RuntimeError("Тип хендлера не поддерживается")

    return handlers


def register_signals(stop_func):
    signal.signal(signal.SIGINT, stop_func)
    # TODO: add more stop signal


def alert_all():
    for team_id in teams:
        teams[team_id].alert(datetime.now())


def main():
    global web, pool, handlers, teams, core, hard, cli, submitter
    web = jury_web.Web()  # TODO: а что тут забыл веб?
    pool = jury_core.ThreadPool()
    handlers = create_handlers(web, pool)
    teams = create_teams(handlers)
    core = jury_core.Core(teams, web)
    cli = jury_cli.JuryShell()
    submitter = jury_flag.FlagSubmitter()

    def signal_to_exit(sig, frame):
        print("\nWait to stop submitter...")
        submitter.stop()
        print("\nWait to stop hard...")
        hard.stop()
        print("\nWait to cleanup teams...")
        for _, team in teams.items():
            team.cleanup()
        print("\nWait to stop handlers...")
        for _, h in handlers.items():
            h.stop()
        print("\nWait to stop pool...")
        pool.stop()
        pool.join()
        print("\nAll done.")
        sys.exit(0)

    def team_win(team_id, flag):
        print(f"""
=================================================

        team: {team_id} won
        flag: {flag}

=================================================
        """)
        os.kill(os.getpid(), signal.SIGINT)  # FIXME: некогда думать с синхронизацией потоков, так что пока так

    hard = None
    if jury_config.Global.ENABLE_HARDWARE_PROXY:
        raise NotImplementedError("proxy not impl")
    elif jury_config.Global.ENABLE_FAKE_HARDWARE:
        from . import proxy as jury_proxy
        hard = jury_proxy.FakeHard(core, pool)
    else:
        from . import hardware as jury_hardware
        hard = jury_hardware.Hard(core)

    core.set_hard(hard)

    logger.info("Launching handlers")
    for _, h in handlers.items():
        h.run()

    logger.info("Launching submitter")
    submitter.start(team_win, pool)

    register_signals(signal_to_exit)

    timer = pool.create_timer(jury_config.Global.TIMER_FOR_RED_ALERT, alert_all)
    timer.start()

    print("Press Ctrl+C to stop")

    cli.cmdloop()
