#!/usr/bin/env python3

import cmd
import os
import traceback
from datetime import datetime
from .. import core as jury_core
from .. import handlers as jury_handlers
from .. import proxy as jury_proxy
from .. import config as jury_config
from .. import run as ctx

if jury_config.Global.ENABLE_HARDWARE_PROXY:
    from .. import hardware as jury_hardware


def nothrow(func):
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except:  # noqa
            traceback.print_exc()
    wrapper.__doc__ = func.__doc__
    return wrapper


class JuryShell(cmd.Cmd):
    intro = 'Redpumpkin shell.\nНаберите "help" или "?" для списка команд.\n'
    prompt = 'jury > '
    file = None

    def emptyline(self):  # ничего не делаем, если ввели пустую строку
        pass

    @nothrow
    def do_list_threads(self, args):
        """Выводить список тредов"""
        for t in ctx.pool.get_list_threads():
            print(f"Поток '{t.name}'\t\tзапущен: {t.is_alive()}")

    @nothrow
    def do_list_timers(self, args):
        """Выводить список таймеров"""
        for timer_id, t in ctx.pool.get_timer_dict().items():
            print(f"Таймер({timer_id}) '{t.name}'\t\tзапущен: {t.is_launched()}\t\tпоследнее срабатывание в: {t.get_last_time_trigger()}")

    @nothrow
    def do_ctrl_timer(self, args: str):
        """Управление таймером
ctrl_timer stop time_id  - останавливает таймер
ctrl_timer start time_id  - останавливает таймер
ctrl_timer restart time_id  - перезапускает таймер
ctrl_timer timeout time_id timeout  - перезапускает таймер с заданным временем в секудах
        """
        args = args.split(' ')
        command = args[0]
        timer = ctx.pool.get_timer_dict()[int(args[1])]
        if command == "stop":
            timer.stop()
        elif command == "start":
            timer.start()
        elif command == "restart":
            timer.stop()
            timer.start()
        elif command == "timeout":
            timeout = int(args[2])
            timer.stop()
            timer.timeout = timeout
            timer.start()
        else:
            raise RuntimeError(f"Command '{command}' not found")

    @nothrow
    def do_list_teams(self, args):
        """Выводит список команд и некоторую информацию о них"""
        for team_id in ctx.teams:
            self.do_get_team(str(team_id))

    @nothrow
    def do_get_team(self, args):
        """Выводит информацию о данной команде"""
        team_id = int(args)
        team = ctx.teams[team_id]
        print("=============")
        print(f"Для команды {team.get_name()} [{team_id}]:")
        idx_btn = team.btn()
        print(f"Индекс кнопки: {idx_btn}")
        print(f"Состояние кнопки: {ctx.hard.get_state_led(idx_btn)}")
        print(f"Состояние в ядре: {team.get_state()}")

        socs = team.get_handler().get_sockets_team(team_id)
        print(f"Обработчиков {len(socs)}:")
        for soc in socs:
            print("  ", soc.getpeername())

    @nothrow
    def do_alert(self, args):
        """Сгенерировать alert для команды"""
        team_id = int(args)
        team = ctx.teams[team_id]
        team.alert(datetime.now())
        idx_btn = team.btn()
        print(f"Состояние кнопки: {ctx.hard.get_state_led(idx_btn)}")
        print(f"Состояние в ядре: {team.get_state()}")

    @nothrow
    def do_emit_click(self, args):
        """Сгенерировать click для команды"""
        team_id = int(args)
        team = ctx.teams[team_id]
        btn_idx = team.btn()
        ctx.core.on_click(btn_idx, False)
        print(f"Состояние кнопки: {ctx.hard.get_state_led(btn_idx)}")
        print(f"Состояние в ядре: {team.get_state()}")

    @nothrow
    def do_follow_log(self, args):
        tail_arg = "-f"
        filelog = os.path.join(os.getcwd(), jury_config.Logger.LOG_FILE_NAME)
        if len(args) > 0:
            tail_arg += ' ' + args

        os.system(f"tail {tail_arg} {filelog}")

    @nothrow
    def do_shell(self, cmd):
        """Исполнить python скрипт, доступны все модули:
jury_core
jury_handlers
jury_hardware
jury_proxy

И контекст run.py под именем ctx
        """
        res = eval(cmd)
        print(f"result: {res}")

    # @nothrow
    # def do_exit(self, args):
    #     print("Exit")
    #     return True
