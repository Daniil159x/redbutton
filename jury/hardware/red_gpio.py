# -*- coding: utf-8 -*-
"""Управление gpio"""

import RPi.GPIO as GPIO
from ..core import log as jury_log

logger = jury_log.get_logger("RedGPIO")


class RedGPIO:
    """Класс предоставляющий управление над 'кнопками'"""

    def __init__(self, config):
        self._local_btns = config.Hard.BUTTONS_COLLECTION
        self._callbacks = [None] * len(self._local_btns)
        self._pin_idx_map = {}

        if self._local_btns is None or len(self._local_btns) == 0:
            logger.error("Не обнаруженно ни одной кнопки")
            return


        GPIO.cleanup()

        GPIO.setmode(GPIO.BCM)
        GPIO.setwarnings(False)

        for idx, control in enumerate(self._local_btns):
            GPIO.setup(control[0], GPIO.OUT)
            GPIO.output(control[0], False)
            self._pin_idx_map[control[0]] = idx
            logger.info(f"{idx}: LED {control[0]} inited")

            GPIO.remove_event_detect(control[1])
            GPIO.setup(control[1], GPIO.IN, pull_up_down=GPIO.PUD_UP)
            GPIO.add_event_detect(control[1], GPIO.RISING, callback=self.__btn_callback, bouncetime=300)
            self._pin_idx_map[control[1]] = idx
            logger.info(f"{idx}: BTN {control[1]} inited")

    def cleanup(self):
        GPIO.cleanup()

    def __del__(self):
        self.cleanup()

    def btns_count(self):
        return len(self._local_btns)

    def turn_on_led(self, idx):
        """Включить кнопку по индексу"""
        GPIO.output(self._local_btns[idx][0], True)

    def turn_off_led(self, idx):
        """Выключить кнопку по индексу"""
        GPIO.output(self._local_btns[idx][0], False)

    def get_state_led(self, idx):
        """Вернуть текущее состояние кнопки по индексу"""
        return GPIO.input(self._local_btns[idx][0])  # WARNING: не проверялось

    def set_btn_listener_on(self, idx, callback):
        """Установить callback для кнопки, None корректен"""
        self._callbacks[idx] = callback

    def __btn_callback(self, pin):
        idx = self._pin_idx_map[pin]
        logger.info(f"Btn on {pin} pin, {idx} idx")
        if idx >= len(self._callbacks) or self._callbacks[idx] is None:
            logger.error(f"Не найден callback для кнопки {idx} на пине {pin}")
            return

        self._callbacks[idx](self.get_state_led(idx))
