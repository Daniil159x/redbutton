# -*- coding: utf-8 -*-
"""Описание класса железной части журейки"""

from .. import config
from .red_gpio import RedGPIO
from ..core import log as jury_log

logger = jury_log.get_logger("Hard")


class Hard:
    """Класс для общения журейки с железом и наоборот"""

    def __init__(self, core):
        self._core = core
        self._gpio = RedGPIO(config)

        for i in range(self._gpio.btns_count()):
            self._gpio.set_btn_listener_on(i, self.__make_callback(i))

    def turn_on_led(self, idx):
        self._gpio.turn_on_led(idx)

    def turn_off_led(self, idx):
        self._gpio.turn_off_led(idx)

    def get_state_led(self, idx):
        return self._gpio.get_state_led(idx)

    def stop(self):
        self._gpio.cleanup()

    def __make_callback(self, idx):
        def callback(current_state):
            logger.debug(f"callback called: idx = {idx}, current_state = {current_state}")
            self._core.on_click(idx, current_state)
        return callback
