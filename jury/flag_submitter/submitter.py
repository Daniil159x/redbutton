#!/usr/bin/env python3

import socket
from typing import Callable
from ..core import log as jury_log
from .. import config as jury_config

logger = jury_log.get_logger("FlagSubmitter", jury_config.DEBUG)


class FlagSubmitter:
    """Флагоприниматель"""
    def __init__(self):
        self._server = socket.socket()
        self._server.settimeout(2)
        self._callback = None
        self._has_stop = False
        self._pool = None
        self._thread = None

    def stop(self):
        self._has_stop = True
        FlagSubmitter.__wrap_shutdown(self._server)

    def start(self, callback: Callable[[int, str], None], pool):
        """Запуск принятия флагов"""
        self._callback = callback
        self._has_stop = False
        self._pool = pool
        self._thread = self._pool.launch_thread(target=self.__loop, name="FlagSubmitter_listen")

    def __loop(self):
        """Принятие соединений"""
        logger.info(f"binding...")
        self._server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self._server.bind(('', jury_config.Flag.BIND_PORT))

        logger.info(f"listening...")
        self._server.listen(1)
        while not self._has_stop:
            try:
                conn, addr = self._server.accept()
                conn.settimeout(2)
                logger.info(f"accept {addr}")
                self._pool.launch_thread(target=self.__recv_socker, args=(conn,), name=f"FlagSubmitter_{conn.getpeername()}")

                logger.info(f"listening...")
                self._server.listen(1)
            except socket.timeout:
                pass
            except Exception as e:
                logger.error(f"accepting: {e}")

                if not self._has_stop:  # FIXME: что за жесть
                    logger.info(f"listening...")
                    self._server.listen(1)

    def __recv_socker(self, conn: socket.socket):
        """Обработка одного соединения"""
        name = conn.getpeername()

        def work():
            try:
                conn.sendall(b"Enter the team number: ")
                msg = self.__wrap_recv(conn)
                if msg is None:
                    conn.sendall(b"Disconect")
                    logger.warning(f"Connect with '{name}' is bad")
                    return

                logger.debug(f"{name}->{{jury}}: '{msg}''")
                team_id, err = self.__try_parse_team_id(msg)

                if team_id is None:
                    conn.sendall(err.encode())
                    logger.info(f"Bad input team: '{msg}'")
                    return

                conn.sendall(b"Enter flag: ")
                flag = self.__wrap_recv(conn)
                if flag is None:
                    conn.sendall(b"Disconect")
                    logger.warning(f"Connect with '{name}' is bad")
                    return
                logger.debug(f"{name}->{{jury}}: '{flag}'")

                if flag in jury_config.Flag.FLAGS:
                    logger.info(f"FLAG ACCEPTED {flag}")
                    conn.sendall(b"Accepted")
                    self._callback(team_id, flag)
                    return
                else:
                    conn.sendall(b"No")

            except socket.timeout:
                logger.debug("timeout")
                pass
            # except Exception as e:
            #     logger.error(f"__recv_socket error: {e}")

        work()
        logger.info(f"connect {name} shutdown")
        FlagSubmitter.__wrap_shutdown(conn)

    def __try_parse_team_id(self, s):
        try:
            team_id = int(s)
            if team_id in jury_config.Team.TEAMS:
                return team_id, None
        except:  # noqa
            return None, "Parse failed"
        return None, "Team not found"

    def __wrap_recv(self, conn: socket.socket):
        while not self._has_stop:
            try:
                return conn.recv(1024).decode().strip()
            except socket.timeout:
                pass
        return None

    @staticmethod
    def __wrap_shutdown(soc: socket.socket, how=socket.SHUT_RDWR):
        try:
            soc.shutdown(how)
        except:  # noqa
            pass
