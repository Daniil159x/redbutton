# -*- coding: utf-8 -*-

from logging import DEBUG, INFO
import platform


class Global:
    """Класс глобальных настроек."""

    REPOSITORY_ROOT_PATH = "../"
    """ Путь до корня репозитория
    Используется для добавления в sys.path и импорта jury-sdk, возможно и других модулей.
    Решение - костыль, кто знает способ нормальный способ
    импортировать модули без создания общего пакета - оставьте feedback
    """

    ENABLE_HARDWARE_PROXY = False
    """ Включение прокси
    Если включен, то между железной частью и ядром журейки будет прокси,
    отвечающее за передачу вызовов по сети.

    Если нет, то вызовы идут напрямую в нужные модули.
    """

    ENABLE_FAKE_HARDWARE = platform.node() != "raspberrypi"
    # ENABLE_FAKE_HARDWARE = True
    """ Фейк железо
    Предназначено для разработки и отладки
    Для функционирования открывает tcp порт и пишет в stdout

    В соединение можно писать только индекс кнопки
    Проверок на валидность нет
    """

    TIMER_FOR_RED_ALERT = 1 * 60
    """Время через которое загораются все кнопки в секундах"""


class FakeHard:
    """Конфигурация фейкового прокси"""

    BIND_PORT = 9999
    """Порт на котором будет открыто соединение"""


class HandlersType:
    """Типы хендлеров (реализаций взаимодействия) сервисов"""

    SOCKET = 0
    """Хендлер на сокетах"""


class SocketHandler:
    """Конфигурация для сокетного хэндлера"""

    BIND_PORT = 30555
    """Порт для бинда слушающего сокета"""


class Team:
    """Класс для перечисления тим"""

    TEAMS = (1111, 2222, 3333)
    """Тупл id команд"""

    TEAM_COLLECTION = {
        TEAMS[0]: "first team",
        TEAMS[1]: "second team",
        TEAMS[2]: "third team",
    }
    """Коллекция тим, состоит из туплов: (id, name)"""

    TEAM_HANDLERS_COLLECTION = {
        TEAMS[0]: HandlersType.SOCKET,
        TEAMS[1]: HandlersType.SOCKET,
        TEAMS[2]: HandlersType.SOCKET
    }


class Hard:
    """Класс настроки железа"""

    LEDS = (18, 24, 16)
    """Вспомогающая константа"""

    BTNS = (17, 23, 12)
    """Вспомогающая константа"""

    BUTTONS_COLLECTION = (
        (LEDS[0], BTNS[0]),
        (LEDS[1], BTNS[1]),
        (LEDS[2], BTNS[2]),
    )
    """Коллекция кнопок

    Состоит из туплов - описание кнопки: (led pin, btn pin)
    """


assert(len(Team.TEAMS) == len(Hard.BUTTONS_COLLECTION))


class Logger:
    """Класс настройки логгера"""

    # DEFAULT_LOGGER_LEVEL = DEBUG
    DEFAULT_LOGGER_LEVEL = INFO
    """Уровень сообщений по умолчанию, которые записывает логгер"""

    LOG_FILE_NAME = "jury.log"
    """Имя файла лога"""

    LOG_FILE_MODE = "w"
    """Мод с которым открывается файл лога"""


class Flag:
    """Класс настройки флагопринимателя"""

    FLAGS = set([
        "RedPumpkin{Have_you_been_drinking?}",  # Artom, I love nginx
        "RedPumpkin{n0_p41N_n0_641N}",  # Pert, drunkenshell
        "RedPumpkin{Them_that_dies_will_be_the_lucky_ones}",
        "RedPumpkin{M-M-M-M-Monster_Kill}",
        "RedPumpkin{Are_you_happy_with_Halloween?}"
    ])
    """Флаги при которых жюрейка завершает свою жизнь"""

    BIND_PORT = 18181
    """Порт на котором будет слушать"""
