
import socket
from .. import config
from ..core import log as jury_log

logger = jury_log.get_logger("FakeHard")


class FakeHard:
    """Класс для общения журейки с железом и наоборот"""

    def __init__(self, core, pool):
        self._core = core
        self._pool = pool
        self._server_socket = socket.socket()
        self._client_socket = None
        self._has_stop = False
        self._pool.launch_thread(target=self.__loop, name="FakeHard_loop")
        self._states = [False] * len(config.Hard.BUTTONS_COLLECTION)

    def turn_on_led(self, idx):
        logger.info(f"led ON on {idx}")
        self._states[idx] = True

    def turn_off_led(self, idx):
        logger.info(f"led OFF on {idx}")
        self._states[idx] = False

    def get_state_led(self, idx):
        return self._states[idx]

    def stop(self):
        self._has_stop = True
        FakeHard.__wrap_shutdown(self._server_socket)
        if self._client_socket is not None:
            FakeHard.__wrap_shutdown(self._client_socket)

    def __accept(self):
        self._server_socket.listen(1)
        self._server_socket.settimeout(2)
        while not self._has_stop:
            try:
                return self._server_socket.accept()
            except socket.timeout:
                pass
            except OSError as e:
                logger.error(f"accept error {e}")
        return None, None

    def __loop(self):
        self._server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self._server_socket.bind(('', config.FakeHard.BIND_PORT))
        while not self._has_stop:
            logger.info(f"listening control connect on {self._server_socket.getsockname()} ...")
            self._client_socket, addr = self.__accept()
            logger.info(f"connect {addr}")
            while not self._has_stop:
                data = self._client_socket.recv(1024)
                if data is None or len(data) == 0:
                    self._client_socket = None
                    break
                logger.info(f"click on {data}")
                try:
                    self._core.on_click(int(data), False)
                except Exception as e:
                    logger.error(f"exception: {e}")

    @staticmethod
    def __wrap_shutdown(soc: socket.socket, how=socket.SHUT_RDWR):
        try:
            soc.shutdown(how)
        except:  # noqa
            pass
